const { createServer } = require('http');
const Parser = require('./parser');

createServer((req, res) => {
  if (req.method === 'POST') {
    const parser = new Parser({ headers: req.headers });

    res.write(JSON.stringify(req.headers));
    let data = '';
    req.setEncoding('utf8');
    req.on('data', chunk => {
      data += chunk;
    });
    req.on('end', () => res.end(data));

    // const parser = new Parser({ headers: req.headers });
    parser.on('file', (fieldname, file, filename, contentType) => {
      // file должен быть Readable stream
      file.on('data', ({ length }) => console.log(`Got ${length} bytes`));
      file.on('end', () => console.log('File finished'));
    });
    parser.on('field', (fieldname, value) => console.log(`${fieldname} ==> ${value}`));
    parser.on('finish', function() {
      console.log('Done parsing form!');
      res.writeHead(200);
      res.end(JSON.stringify('{ success: true }'));
    });
    req.pipe(parser);
  } else if (req.method === 'GET') {
    res.writeHead(200, { Connection: 'close' });
    res.end('OK');
  }
}).listen(process.env.PORT || 8000, () => console.log('Listening'));
