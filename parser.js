const EventEmitter = require('events');
const { Writable, Readable } = require('stream');

class Parser extends Writable {
  constructor({ headers }) {
    super();
    this.headers = headers;
    this.separator = this.getSeparator();
    this.oldChunk = Buffer.alloc(0);

    // console.log(this.headers);
    // console.log(this.separator);
  }

  getSeparator() {
    const contentType = this.headers['content-type'].split(';')[0];
    if (contentType === 'multipart/form-data') {
      return '--' + this.headers['content-type'].split('boundary=')[1];
    } else {
      return null;
    }
  }

  sendField(str) {}

  chunkToFields(chunk) {
    return chunk
      .toString('utf8')
      .split(this.separator)
      .filter(str => str.includes('Content-Disposition'));
  }

  parseContentDisposition(contentDisposition) {
    let output = {};

    contentDisposition
      .split('; ')
      .filter(s => s.includes('='))
      .forEach(data => {
        const [key, val] = data.split('=');
        output[key] = [...val].filter(s => s !== '"').join('');
      });

    return output;
  }

  parseHeaders(headers) {
    let output = {};

    headers
      .split('\r\n')
      .filter(v => v.includes(':'))
      .forEach(str => {
        const [key, val] = str.split(': ');
        output[key] = val;
      });

    return output;
  }

  _write(chunk, enc, next) {
    const arr = this.chunkToFields(chunk);

    if (!arr.length) this.activeFileStream.push(chunk);

    arr.forEach(data => {
      const headers = this.parseHeaders(data.split('\r\n\r\n')[0]);
      const value = data.split('\r\n\r\n')[1];
      const contentDisposition = this.parseContentDisposition(headers['Content-Disposition']);

      const fieldname = contentDisposition.name;

      if (!headers['Content-Type']) {
        this.emit('field', fieldname, value);
      } else {
        this.activeFileStream = new Readable({ read() {} });

        const file = this.activeFileStream;
        const filename = contentDisposition.filename;
        const contentType = headers['Content-Type'];

        if (value) this.activeFileStream.push(value);

        this.emit('file', fieldname, file, filename, contentType);
      }
      // console.log('[headers,value]', [headers, value]);
      // console.log('data ', [data]);
      // const info = this.parseContentDisposition(headers);
      // console.log(headers, value);
      // const [type, name] = contentDisposition.split('; ');
      // console.log('name: ', name);
      // console.log('type: ', type);
    });

    console.log(chunk);

    next();
    // console.log(arr);
    // console.log(chunk.toString());
  }
}

module.exports = Parser;
